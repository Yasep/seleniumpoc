package selenium.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class NoBrowserSalesForce {

	/**
	 * This method is used to get the data from ARIBA page and create a job with
	 * the same data
	 * 
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void test() throws InterruptedException, IOException {

		// WebDriver driver = new HtmlUnitDriver();
		HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_38);
		driver.setJavascriptEnabled(true);

		final WebDriverWait wait = new WebDriverWait(driver, 15);

		// WebDriver driver = new H;

		// driver.close();
		// driver.quit();

		driver.manage().window().maximize();
		driver.get("https://login.salesforce.com/");

		driver.findElement(By.name("username")).sendKeys("yukhnevski@gmail.com");

		// driver.findElementByName("password").sendKeys("Medpro456");
		driver.findElement(By.id("password")).sendKeys("Yasep2133Yasep");

		// driver.findElementByXPath("//button[@value='Sign In']").click();
		driver.findElement(By.id("Login")).click();
		// driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
		WaitTool.waitForElement(driver, By.xpath("//*[@id='AllTab_Tab']/a"), 20);
		//
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='AllTab_Tab']/a")));
		driver.findElement(By.xpath("//*[@id='AllTab_Tab']/a")).click();
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[href$='/015/o']")));
		WaitTool.waitForElement(driver, By.cssSelector("a[href$='/015/o']"), 15);
		driver.findElement(By.cssSelector("a[href$='/015/o']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".pbBody
		// .dataRow")));
		WaitTool.waitForElement(driver, By.cssSelector(".pbBody .dataRow"), 15);
		List<WebElement> dataRows = driver.findElements(By.cssSelector(".pbBody .dataRow"));

		for (WebElement element : dataRows) {
			element.findElement(By.cssSelector("a")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			FileDownloader downloader = new FileDownloader(driver);
			try {
				downloader.downloader(driver.findElement(By.xpath("//td/a[text()='View file']")), "href");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			driver.findElement(By.xpath("//td/a[text()='View file']")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			// URL url = new
			// URL("https://c.ap2.content.force.com/servlet/servlet.FileDownload?file=01528000000j333");
			// InputStream in = new BufferedInputStream(url.openStream());
			// OutputStream out = new BufferedOutputStream(new
			// FileOutputStream("D://temp/download.txt"));

			// for (int i; (i = in.read()) != -1;) {
			// out.write(i);
			// }
			// in.close();
			// out.close();
			// driver.navigate().back();
		}

		System.out.println("DONE");

	}
}
