package selenium.test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class NoBrowserFieldGlass {

	/**
	 * This method is used to get the data from ARIBA page and create a job with
	 * the same data
	 * 
	 * @throws InterruptedException
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void test() throws InterruptedException, IOException {
		// ScreenShot ss=new ScreenShot();
		// TargetRecruit tr=new TargetRecruit();
		System.setProperty("webdriver.chrome.driver", "D:\\distibutive\\chromedriver.exe");

		// WebDriver driver = new HtmlUnitDriver();
		HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX_38);
		driver.setJavascriptEnabled(true);
		// WebDriver driver = new H;

		// driver.close();
		// driver.quit();

		driver.manage().window().maximize();
		driver.get("https://www.fieldglass.net/");
		// driver.findElementByName("username").sendKeys("hlucarelli");
		driver.findElement(By.name("username")).sendKeys("hlucarelli");

		// driver.findElementByName("password").sendKeys("Medpro456");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Medpro456");

		// driver.findElementByXPath("//button[@value='Sign In']").click();
		driver.findElement(By.xpath("//button[@value='Sign In']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// driver.findElementByXPath("//a[text()='View']").click();
		driver.findElement(By.xpath("//a[text()='View']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// driver.findElementByXPath("//a[text()='Job Posting']").click();
		driver.findElement(By.xpath("//a[text()='Job Posting']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// int size=driver.findElementsByXPath("//tbody//nobr/a").size();
		int size = driver.findElements(By.xpath("//tbody//nobr/a")).size();
		System.out.println("Total job record= " + size);

		// driver.findElementByXPath("(//tbody//nobr/a)[1]").click();
		driver.findElement(By.xpath("(//tbody//nobr/a)[1]")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// String tittle=driver.findElementByXPath("//h2/div").getText();
		String tittle = driver.findElement(By.xpath("//h2/div")).getText();

		// String
		// period=driver.findElementByXPath("//div[nobr[text()='Period']]/following-sibling::div").getText();
		String period = driver.findElement(By.xpath("//div[nobr[text()='Period']]/following-sibling::div")).getText();

		// String
		// status=driver.findElementByXPath("//div[nobr[text()='Status']]/following-sibling::div").getText();
		String status = driver.findElement(By.xpath("//div[nobr[text()='Status']]/following-sibling::div")).getText();

		// String
		// buyer=driver.findElementByXPath("//div[nobr[text()='Buyer']]/following-sibling::div").getText();
		String buyer = driver.findElement(By.xpath("//div[nobr[text()='Buyer']]/following-sibling::div")).getText();

		// String
		// specialty=driver.findElementByXPath("//th[table[tbody[tr[td[text()='Specialty']]]]]/following-sibling::td").getText();
		// String
		// specialty=driver.findElement(By.xpath("//th[table[tbody[tr[td[text()='Specialty']]]]]/following-sibling::td")).getText();

		// String
		// description=driver.findElementByXPath("//tr[th[text()='Description']]/following-sibling::tr[1]//div").getText();
		String description = driver
				.findElement(By.xpath("//tr[th[text()='Description']]/following-sibling::tr[1]//div")).getText();

		// String jobPosting=driver.findElementByXPath("//li[text()='Job
		// Posting']/following-sibling::li").getText();
		String jobPosting = driver.findElement(By.xpath("//li[text()='Job Posting']/following-sibling::li")).getText();

		System.out.println("Tittle= " + tittle);
		System.out.println("Period= " + period);
		System.out.println("Status= " + status);
		System.out.println("Buyer= " + buyer);
		// System.out.println("Specialty= "+specialty);
		System.out.println("Description= " + description);
		System.out.println("Job Posting= " + jobPosting);
		// ss.screenShots(driver, "screen shot of "+jobPosting+" Field Glass
		// "+status);

		// driver.findElementByXPath("//a[text()='Sign Out']").click();
		// driver.findElement(By.xpath("//a[text()='Sign Out']")).click();

		// tr.jobcreation(driver, tittle, "Field Glass", jobPosting,
		// description, "postingType", status, period, buyer, specialty);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://login.salesforce.com");

		// driver.findElementByName("username").sendKeys("santosh.j@targetrecruit.net");
		driver.findElement(By.name("username")).sendKeys("santosh.j@targetrecruit.net");

		// driver.findElementByName("pw").sendKeys("avankia2");
		driver.findElement(By.name("pw")).sendKeys("avankia2");

		// driver.findElementByName("Login").click();
		driver.findElement(By.name("Login")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Thread.sleep(1000);

		// driver.findElementByXPath("//a[@href='/home/showAllTabs.jsp']").click();
		// driver.findElementByXPath("//img[@class='allTabsArrow']").click();
		driver.findElement(By.xpath("//img[@class='allTabsArrow']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// driver.findElementByLinkText("Jobs").click();
		driver.findElement(By.linkText("Jobs")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// driver.findElementByXPath("//td[@class='pbButton']/input[@value=' New
		// ']").click();
		driver.findElement(By.xpath("//td[@class='pbButton']/input[@value=' New ']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// driver.findElementByXPath("//td[label[text()='Job
		// Title']]/following-sibling::td[1]/input").sendKeys(tittle);
		driver.findElement(By.xpath("//td[label[text()='Job Title']]/following-sibling::td[1]/input")).sendKeys(tittle);

		// driver.findElementByXPath("//td[label[text()='VMS
		// Name']]/following-sibling::td//input").sendKeys("Field Glass");
		driver.findElement(By.xpath("//td[label[text()='VMS Name']]/following-sibling::td//input"))
				.sendKeys("Field Glass");

		// driver.findElementByXPath("//td[label[text()='VMS Job
		// ID']]/following-sibling::td//input").sendKeys(jobPosting);
		driver.findElement(By.xpath("//td[label[text()='VMS Job ID']]/following-sibling::td//input"))
				.sendKeys(jobPosting);

		// driver.findElementByXPath("//td[label[text()='VMS Job
		// Type']]/following-sibling::td//input").sendKeys("postingType");
		driver.findElement(By.xpath("//td[label[text()='VMS Job Type']]/following-sibling::td//input"))
				.sendKeys("postingType");

		// driver.findElementByXPath("//td[label[text()='Period']]/following-sibling::td//input").sendKeys(period);
		driver.findElement(By.xpath("//td[label[text()='Period']]/following-sibling::td//input")).sendKeys(period);

		// driver.findElementByXPath("//td[label[text()='Buyer']]/following-sibling::td//input").sendKeys(buyer);
		driver.findElement(By.xpath("//td[label[text()='Buyer']]/following-sibling::td//input")).sendKeys(buyer);

		// driver.findElementByXPath("//td[label[text()='Specialty']]/following-sibling::td//input").sendKeys(specialty);
		// driver.findElement(By.xpath("//td[label[text()='Specialty']]/following-sibling::td//input")).sendKeys(specialty);

		// Select sel=new
		// Select(driver.findElementByXPath("//td[label[text()='Stage']]/following-sibling::td//select"));
		Select sel = new Select(
				driver.findElement(By.xpath("//td[label[text()='Stage']]/following-sibling::td//select")));
		sel.selectByVisibleText(status);

		// driver.findElementByXPath("//td[label[text()='Posting
		// Summary']]/following-sibling::td/textarea").sendKeys(description);
		driver.findElement(By.xpath("//td[label[text()='Posting Summary']]/following-sibling::td/textarea"))
				.sendKeys(description);

		// driver.findElementByXPath("//td[@id='topButtonRow']/input[@value='
		// Save ']").click();
		driver.findElement(By.xpath("//td[@id='topButtonRow']/input[@value=' Save ']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// get the total memory for my app
		long total = Runtime.getRuntime().totalMemory();
		// get the free memory available
		long free = Runtime.getRuntime().freeMemory();

		// some simple arithmetic to see how much i use
		long used = total - free;

		System.out.println("Total memory" + total);
		System.out.println("Free memory" + free);
		System.out.println("Used memory in bytes: " + used);
		System.out.println("--------- Execution completed. -----------");

	}
}
