package com.mydbsync.selenium.entities;

public interface ProcessorConstants {
	public final static String CLICK_ACTION_ATTRIBUTE = "click";
	public final static String FORWARD_ACTION_ATTRIBUTE = "forward";
	public final static String GET_DATA_ATTRIBUTE = "getdata";
	public final static String ID_ATTRIBUTE = "id";
	public final static String XPATH_ATTRIBUTE = "xpath";
	public final static String URL_ATTRIBUTE = "url";
	public final static String USER_NAME_FIELD_PATH_ATTRIBUTE = "usernameFieldId";
	public final static String USERNAME_ATTRIBUTE = "username";
	public final static String PASSWORD_FIELD_PATH_ATTRIBUTE = "passwordFieldId";
	public final static String PASSWORD_ATTRIBUTE = "password";
	public final static String RESULT_TYPE_ATTRIBUTE = "resulttype";
	public final static String PAGE_RESULT_TYPE_ATTRIBUTE = "page";
	public final static String PLAIN_DATA = "data";
	public final static String STREAM_DATA = "download";
}
