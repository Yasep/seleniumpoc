package com.mydbsync.selenium.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"properties"})
public class Column {

	@JsonProperty("properties")
	private HashMap<String, String> properties = new HashMap<>();
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	@JsonAnyGetter
    public Map<String, String> getProperties() {
        return properties;
    }

    @SuppressWarnings("unchecked")
	@JsonAnySetter
    public void setProperties(Object property) {
        if(property instanceof ArrayList<?>){
        	List<?> propertiesList = (ArrayList<?>)property;
        	for (Object currentPropertyObject : propertiesList) {
				if(currentPropertyObject instanceof Map) {
					Map<String, String> currentProperty = (Map<String, String>) currentPropertyObject;
					this.properties.put(currentProperty.get("name"), currentProperty.get("value"));
				}
			}
        }
    }

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}