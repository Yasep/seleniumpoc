package com.mydbsync.selenium.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "type", "properties" })
public class Action {
    @JsonProperty("type")
    private String type;
    @JsonProperty("url")
    private String url;
    @JsonProperty("xpath")
    private String xpath;
    @JsonProperty("id")
    private String id;
    @JsonProperty("resulttype")
    private String resulttype;
    @JsonProperty("properties")
    private HashMap<String, String> properties = new HashMap<>();
    @JsonProperty("columns")
    private List<Column> columns = new ArrayList<Column>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
    @JsonProperty("type")
    public String getType() {
        return type;
    }
    
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getXpath() {
        return xpath;
    }
    
    public void setXpath(String xpath) {
        this.xpath = xpath;
    }
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getResulttype() {
        return resulttype;
    }
    
    public void setResulttype(String resulttype) {
        this.resulttype = resulttype;
    }
    
    @JsonAnyGetter
    public Map<String, String> getProperties() {
        return properties;
    }
    
    public String getPropertyValue(String key) {
        return properties.get(key);
    }
    
    public String setPropertyValue(String key, String value) {
        return properties.put(key, value);
    }
    
    @SuppressWarnings("unchecked")
    @JsonAnySetter
    public void setProperties(Object property) {
        if (property instanceof ArrayList<?>) {
            List<?> propertiesList = (ArrayList<?>) property;
            for (Object currentPropertyObject : propertiesList) {
                if (currentPropertyObject instanceof Map) {
                    Map<String, String> currentProperty = (Map<String, String>) currentPropertyObject;
                    this.properties.put(currentProperty.get("name"), currentProperty.get("value"));
                }
            }
        }
    }
    
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }
    
    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}