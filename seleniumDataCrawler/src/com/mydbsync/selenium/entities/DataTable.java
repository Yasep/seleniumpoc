package com.mydbsync.selenium.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DataTable {
	
	private List<Map<String, Object>> data;
	
	public DataTable() {
		data = new ArrayList<>();
	}
	
	public void addRow(Map<String, Object> row) {
		data.add(row);
	}
	
	public List<Map<String, Object>> getAllRows() {
		return data;
	}
}
