package com.mydbsync.selenium.processing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.mydbsync.selenium.driver.CustomHtmlUnitDriver;
import com.mydbsync.selenium.entities.Action;
import com.mydbsync.selenium.entities.ParserParameters;
import com.mydbsync.selenium.entities.ProcessorConstants;

public class SeleniumCrawler {

	private final static Logger LOGGER = Logger.getLogger(SeleniumCrawler.class);

	final List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

	public List<Map<String, Object>> getResult() {
		return result;
	}

	private Properties properties;

	public static void main(String[] args) {

		try {
			File test = new File("D:\\repositories\\seleniumpoc\\seleniumDataCrawler\\sample.json");
			StringBuilder json = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(test));
			while (reader.ready()) {
				json.append(reader.readLine());
			}
			reader.close();

			ParserParameters input = ParserParametersProcessor.parseRequest(json.toString());
			Properties props = new Properties();
			props.put("username", "mikalai.zhukouski@mydbsync.com");
			props.put("password", "svena860");
			SeleniumCrawler crawler = new SeleniumCrawler(props);

			crawler.scrapData(input);
		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public SeleniumCrawler() {
	}

	public SeleniumCrawler(Properties properties) {
		this.properties = properties;
	}

	private static boolean isEmptyString(String string) {
		return string == null || string.isEmpty();
	}

	public void scrapData(String script) {
		try {
			ParserParameters input = ParserParametersProcessor.parseRequest(script);
			this.scrapData(input);
		} catch (Exception e) {
			LOGGER.error("Error scrapping data!", e);
		}
	}

	private void scrapData(ParserParameters input) throws FailingHttpStatusCodeException, MalformedURLException, IOException {

		final CustomHtmlUnitDriver driver = new CustomHtmlUnitDriver(BrowserVersion.FIREFOX_38);
		driver.setJavascriptEnabled(true);

		for (Action action : input.getActions()) {
			if (ProcessorConstants.PAGE_RESULT_TYPE_ATTRIBUTE.equals(action.getPropertyValue(ProcessorConstants.RESULT_TYPE_ATTRIBUTE))) {
				processNavigationAction(driver, action);
			}
			final Map<String, Object> elementDataMap = new HashMap<>();

			processElement(driver, action, elementDataMap);

		}
		System.out.println("DONE");

	}

	private void processNavigationAction(CustomHtmlUnitDriver driver, Action action) {
		WebElement webElement = null;
		// element selector block
		final String targetElementId = action.getId();
		final String targetElementXpath = action.getXpath();
		try {
			webElement = driver.findElement(By.id(targetElementId));
		} catch (Exception ex) {
			try {
				webElement = driver.findElement(By.xpath(targetElementXpath));
			} catch (Exception ex1) {
				LOGGER.error("Target element not found!!!", ex1);
			}
		}

		// element action block
		final String actionType = action.getType();
		if (ProcessorConstants.CLICK_ACTION_ATTRIBUTE.equalsIgnoreCase(action.getType())) {

			Map<String, String> actionProperties = action.getProperties();
			if (!actionProperties.isEmpty()) {
				driver.get(action.getPropertyValue(ProcessorConstants.URL_ATTRIBUTE));
				Set<String> properties = action.getProperties().keySet();
				for (String propertyName : properties) {
					WebElement elementToSetValue = driver.findElement(By.id(propertyName));
					if (elementToSetValue != null) {
						elementToSetValue.sendKeys(this.properties.getProperty(propertyName));
						continue;
					}
					elementToSetValue = driver.findElement(By.xpath(propertyName));
					if (elementToSetValue != null) {
						elementToSetValue.sendKeys(this.properties.getProperty(propertyName));
					}
				}
			}
		}

		if (actionType.equalsIgnoreCase(ProcessorConstants.CLICK_ACTION_ATTRIBUTE)) {
			webElement.click();
		} else if (actionType.equalsIgnoreCase(ProcessorConstants.GET_DATA_ATTRIBUTE)) {
			driver.get(action.getPropertyValue(ProcessorConstants.URL_ATTRIBUTE));
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return;
	}

	private void processElement(CustomHtmlUnitDriver driver, Action action, Map<String, Object> rowData)
			throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		/*
		 * WebElement webElement = null; // element selector block if
		 * (!isEmptyString(action.getPropertyValue(ProcessorConstants.
		 * ID_ATTRIBUTE))) { webElement =
		 * driver.findElement(By.id(action.getPropertyValue(ProcessorConstants.
		 * ID_ATTRIBUTE))); } else if (!isEmptyString(action.getXpath())) {
		 * webElement = driver.findElement(By.xpath(action.getXpath())); }
		 * 
		 * if (!isEmptyString(action.getProp())) { // TODO implement
		 * PropertiesHelper String propValue =
		 * properties.getProperty(action.getProp());
		 * 
		 * webElement.sendKeys(propValue); } // element action block if
		 * (action.getType() != null) { final String actionType =
		 * action.getType();
		 * 
		 * if (actionType.equalsIgnoreCase(ProcessorConstants.
		 * CLICK_ACTION_ATTRIBUTE)) { webElement.click(); } else if
		 * (actionType.equalsIgnoreCase(ProcessorConstants.GET_DATA_ATTRIBUTE))
		 * {
		 * driver.get(action.getPropertyValue(ProcessorConstants.URL_ATTRIBUTE))
		 * ; } else if (action.getType().equalsIgnoreCase("getText")) {
		 * 
		 * final String text = webElement.getText(); // TODO cast to dataType
		 * rowData.put(action.getParams().get(ScrapperConstants.
		 * FIELD_NAME_ATTRIBUTE), text); } else if
		 * (action.getType().equalsIgnoreCase("download")) {
		 * 
		 * String href = webElement.getAttribute("href"); final WebClient
		 * webClient = driver.getCustomWebClient(); webElement.click(); final
		 * Page dowloadPage = webClient.getPage(href);
		 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 * final WebResponse fileDLWebResponse = dowloadPage.getWebResponse();
		 * // TODO Add method to get path to the data // final String fileName =
		 * fileDLWebResponse.
		 * rowData.put(action.getParams().get(ScrapperConstants.
		 * FIELD_NAME_ATTRIBUTE), fileDLWebResponse.getContentAsStream()); final
		 * String data = IOUtils.toString(); System.out.println(data); }
		 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 * return; } // element data block if (action.getData() != null) { final
		 * Element nextButton = action.getData().getNextButton(); DataRow row =
		 * action.getData().getRow(); String path =
		 * row.getColumns().get(0).getXpath(); List<WebElement> dataCells =
		 * driver.findElementsByXPath(path);
		 * 
		 * while (dataCells != null && !dataCells.isEmpty()) { for (int i = 0; i
		 * < dataCells.size(); i++) { rowData = new HashMap<>();
		 * 
		 * for (Element cell : row.getColumns()) { processElement(driver, cell,
		 * rowData); }
		 * 
		 * result.add(rowData); } // go to the next page processElement(driver,
		 * nextButton, null); } }
		 */
	}
}
