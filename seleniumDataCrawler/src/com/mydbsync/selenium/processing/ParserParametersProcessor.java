package com.mydbsync.selenium.processing;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.mydbsync.selenium.entities.ParserParameters;

public class ParserParametersProcessor {

	public static ParserParameters parseRequest(final String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();

		ParserParameters elemets = mapper.readValue(json, ParserParameters.class);
		return elemets;
	}
}
