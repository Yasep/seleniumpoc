package com.mydbsync.selenium.interfaces;

import java.util.List;

public interface ActionHandler {
	public List<String> getDataLinks();
}
