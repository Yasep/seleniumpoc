package selenium.test;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;

public class CustomHtmlUnitDriver extends HtmlUnitDriver {
	
	public CustomHtmlUnitDriver(BrowserVersion version) {
		super(version);
	}
	
	@Override
	protected WebClient getWebClient() {
		return super.getWebClient();
	}
}
