package selenium.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.gargoylesoftware.htmlunit.WebClient;
import com.mydbsync.selenium.processing.SeleniumCrawler;

import selenium.test.interfaces.ScreenScrapper;

public class ScreenScrapperImpl<T> implements ScreenScrapper<T> {

	private WebClient webClient;
	
	private Map<String, Object> properties;
	
	SeleniumCrawler crawler;

	public ScreenScrapperImpl(Map<String, Object> properties) {
		this.properties = properties;
		Properties props = new Properties();
		props.putAll(properties);
		this.crawler = new SeleniumCrawler(props);
	}

	@Override
	public List<T> populateRecords(String scenario) {
		List<T> toReturn = new ArrayList<>();
		// TODO Extract record URL from JSON
		String url = "";
		toReturn.add(populateSingleRecord(url));
		return null;
	}

	@Override
	public T populateRecord(String scenario) {
		// TODO Extract record URL from JSON
		String url = "";
		return populateSingleRecord(url);
	}

	private T populateSingleRecord(String url) {
		return null;
	}

	@Override
	public File populateFile(String scenario) {
		this.crawler.scrapData(scenario);
		return null;
	}
}
