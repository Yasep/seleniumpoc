package selenium.test.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DataMapper<T> {
	
	private static Log logger = LogFactory.getLog(DataMapper.class);

	@SuppressWarnings("unchecked")
	public T dataToClass(Class<?> clazz, Map<String, Object> data) {
		T instance = null;
		try {
			final Field[] fields = clazz.getDeclaredFields();
			instance = (T) clazz.newInstance();
			for (int i = 0; i < fields.length; i++) {
				final Field currentField = fields[i];
				final String fieldName = currentField.getName();
				final Object fieldValue = data.get(fieldName);
				currentField.set(instance, fieldValue);
			}
		} catch (Exception e) {
			logger.error("Error while populating data from result to class", e);
		} 
		
		return instance;
	}
	
	public List<T> dataListToClassList(Class<?> clazz, List<Map<String, Object>> data) {
		List<T> toReturn = new ArrayList<T>();
		for (Map<String, Object> rowData : data) {
			toReturn.add(dataToClass(clazz, rowData));
		}
		
		return toReturn;
	}
}
