package selenium.test.utils;

public interface ScrapperConstants {

	public final static String FIELD_NAME_ATTRIBUTE = "fieldName"; 
	public final static String CLICK_ACTION_ATTRIBUTE = "click"; 
}
