package selenium.test;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;

public class NoBrowserSalesForce {

	private static final String LOGIN_PAGE_URL = "https://login.salesforce.com/";
	private static final String PASSWORD = "Yasep2133Yasep";
	private static final String USERNAME = "yukhnevski@gmail.com";

	public void test() throws InterruptedException, IOException {

		// WebDriver driver = new HtmlUnitDriver();
		CustomHtmlUnitDriver driver = new CustomHtmlUnitDriver(BrowserVersion.FIREFOX_38);
		driver.setJavascriptEnabled(true);

		try (final WebClient webClient = driver.getWebClient()) {
			// webClient.getCookieManager().setCookiesEnabled(true);
			// webClient.getOptions().setJavaScriptEnabled(true);

			driver.manage().window().maximize();
			driver.get(LOGIN_PAGE_URL);
			driver.findElement(By.id("username")).sendKeys(USERNAME);

			driver.findElement(By.id("password")).sendKeys(PASSWORD);

			// driver.findElementByXPath("//button[@value='Sign In']").click();
			driver.findElement(By.id("Login")).click();
			driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);

			driver.findElement(By.xpath("(//a[text()='Contacts'])[1]")).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			driver.findElementByXPath("//input[@name='go']").click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			List<WebElement> elements = driver.findElements(By.xpath("//table[@class='x-grid3-row-table']"));
			// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			for (int i = 1; i < elements.size() + 1; i++) {
				String id = driver
						.findElement(
								By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-LIST_RECORD_ID'])[" + i + "]"))
						.getText();
				// TODO get div text
				// System.out.println(id);

				String name = driver
						.findElement(By
								.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-FULL_NAME']/a/span)[" + i + "]"))
						.getText();
				String account = driver
						.findElement(
								By.xpath("(//div[@class='x-grid3-cell-inner x-grid3-col-Account']/a/span)[" + i + "]"))
						.getText();
				System.out.println(id + " | " + name + " | " + account);
			}

			System.out.println("{УЙ");

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("DONE");

	}

}
