package selenium.test.interfaces;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface ScreenScrapper<T> {
	
	/**
	 * Method to set up initial parameters
	 * @param properties
	 */
//	public void init(Map<String, Object> properties);
	
	public List<T> populateRecords(String scenario);

	public T populateRecord(String scenario);

	public File populateFile(String scenario);
	
}
